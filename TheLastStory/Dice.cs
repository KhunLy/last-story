﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Dice
    {
        Random dice = new Random();
        public int Max { get; private set; }
        public int Min { get; private set; }

        public Dice(int max)
        {
            this.Max = max;
            this.Min = 1;
        }

        public int ThrowDice ()
        {
            return dice.Next(Min, Max + 1);
        }

        public int Roll4Dices()
        {
            int[] list = new int[4];
            for (int i = 0; i < 4; i++)
            {
                list[i] = this.ThrowDice();
            }
            Array.Sort(list);
            return list[3] + list[2] + list[1];
        }
    }
}
