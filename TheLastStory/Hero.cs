﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TheLastStory.models
{
    abstract class Hero : Character
    {
        public MediaPlayer MP { get; set; }
        public int Wallet { get; set; }
        public int Bag { get; set; }
        public int Exp { get; set; }
        public List<Accessory> Accessories { get; }

        public Hero (string name, int stamina, int strenght) : base(name, stamina, strenght)
        {
            this.Wallet = 0;
            this.Bag = 0;
            this.X = 7;
            this.Y = 14;
            this.Exp = 0;
            this.Initial = "☺";
            this.MP = new MediaPlayer();
            this.Accessories = new List<Accessory>();
        }

        public virtual void Restore()
        {
            this.CurrentHP = this.HP;
        }

        public virtual void Loot(Monster m)
        {
            if (m is IOr)
            {
                Wallet += ((IOr)m).Or;
            }
            if (m is ICuir)
            {
                Bag += ((ICuir)m).Cuir;
            }
            Lib.DisplayLoot(this);
            MP.Open(new Uri("../../music/coin.wav", UriKind.Relative));
            MP.Play();
            m.Loot = false;
        }

        public Boolean NextTo(IPosition m)
        {
            if (((m.X == this.X) && (Math.Abs(m.Y - this.Y) <= 1)) || ((m.Y == this.Y) && (Math.Abs(m.X - this.X)) <= 1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Equip(IEquipment e)
        {
            if (e is Sword)
            {
                this.Weapon = e;
                this.Display();
                this.DamageSup = e.AttackDamage;
            }
        }

        public Boolean GoToWall(ConsoleKey k, IWall w)
        {
            switch (k)
            {
                case ConsoleKey.UpArrow:
                    if (w.X == this.X && w.Y == this.Y - 1)
                        return true;
                    else
                        return false;
                case ConsoleKey.DownArrow:
                    if (w.X == this.X && w.Y == this.Y + 1)
                        return true;
                    else
                        return false;
                case ConsoleKey.LeftArrow:
                    if (w.X == this.X - 1 && w.Y == this.Y)
                        return true;
                    else
                        return false;
                case ConsoleKey.RightArrow:
                    if (w.X == this.X + 1 && w.Y == this.Y)
                        return true;
                    else
                        return false;
                default:
                    return true;
            }
        }

        public Boolean Move(ConsoleKey k) 
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            if (k == ConsoleKey.UpArrow)
            {
                if (this.Y - 1 < 0)
                {
                    return false;
                }
                else
                {
                    Console.SetCursorPosition((this.X) * 3 + 8, (this.Y) * 2 + 12);
                    Console.Write(" ");
                    this.Y = this.Y - 1;
                    Console.SetCursorPosition((this.X) * 3 + 8, (this.Y) * 2 + 12);
                    Console.Write("☺");
                    return true;
                }
            }
            else if (k == ConsoleKey.RightArrow)
            {
                if (this.X + 1 > 14)
                {
                    return false;
                }
                else
                {
                    Console.SetCursorPosition((this.X) * 3 + 8, (this.Y) * 2 + 12);
                    Console.Write(" ");
                    this.X = this.X + 1;
                    Console.SetCursorPosition((this.X) * 3 + 8, (this.Y) * 2 + 12);
                    Console.Write("☺");
                    return true;
                }
            }
            else if (k == ConsoleKey.DownArrow)
            {
                if (this.Y + 1 > 14)
                {
                    return false;
                }
                else
                {
                    Console.SetCursorPosition((this.X) * 3 + 8, (this.Y) * 2 + 12);
                    Console.Write(" ");
                    this.Y = this.Y + 1;
                    Console.SetCursorPosition((this.X) * 3 + 8, (this.Y) * 2 + 12);
                    Console.Write("☺");
                    return true;
                }
            }
            else if (k == ConsoleKey.LeftArrow)
            {
                if (this.X - 1 < 0)
                {
                    return false;
                }
                else
                {
                    Console.SetCursorPosition((this.X) * 3 + 8, (this.Y) * 2 + 12);
                    Console.Write(" ");
                    this.X = this.X - 1;
                    Console.SetCursorPosition((this.X) * 3 + 8, (this.Y) * 2 + 12);
                    Console.Write("☺");
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        public Boolean On(IPosition m)
        {
            if ((m.X == this.X) && (m.Y == this.Y))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Display()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.SetCursorPosition(65, 12);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(65, 12);
            Console.WriteLine(this.Name);
            Console.SetCursorPosition(65, 13);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(65, 13);
            Console.WriteLine(this.Type);
            Console.SetCursorPosition(69, 14);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(69, 14);
            Console.WriteLine(this.Strenght);
            Console.SetCursorPosition(68, 15);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(68, 15);
            Console.WriteLine(this.Stamina);
            Console.SetCursorPosition(63, 17);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(63, 17);
            Console.WriteLine(this.Exp);
            Lib.DisplayCurrentHP(this);
            Lib.DisplayLoot(this);
            if (this.Weapon != null)
            {
                Console.SetCursorPosition(67, 25);
                Console.WriteLine("                    ");
                Console.SetCursorPosition(67, 25);
                Console.WriteLine($"{this.Weapon.Name} + {this.Weapon.AttackDamage} AD" );
            }
            

        }
        public void AddAccessory(Accessory a)
        {
            this.Accessories.Add(a);
        }
    }
}
