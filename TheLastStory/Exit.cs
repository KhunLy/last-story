﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Exit : IPosition
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Map OutMap { get; set; }
        public String Initial { get; set; }

        public Exit(int x, int y, Map outMap/*, Map inMap*/)
        {
            this.X = x;
            this.Y = y;
            this.OutMap = outMap;
            //this.InMap = inMap;
            
        }
    }
}
