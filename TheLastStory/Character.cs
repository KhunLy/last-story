﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    abstract class Character : ICharacter, IPosition
    {
        public int Stamina { get; private set; }
        public int InitialStrenght { get; private set; }
        public int Strenght { get; set; }
        public int HP { get; private set; }
        public int CurrentHP { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int DamageSup { get; set; }
        public int DefenseSup { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Initial { get; set; }
        public IEquipment Weapon { get; set; }

        public Character(string name, int stamina, int strenght)
        {
            this.DamageSup = 0;
            this.DefenseSup = 0;
            this.Name = name;
            this.Stamina = stamina;
            this.InitialStrenght = strenght;
            this.Strenght = this.InitialStrenght;
            if (this.Stamina <= 5)
            {
                this.HP = this.Stamina - 1;
            }
            else if (this.Stamina <= 10)
            {
                this.HP = this.Stamina;
            }
            else if (this.Stamina <= 15)
            {
                this.HP = this.Stamina + 1;
            }
            else
            {
                this.HP = this.Stamina + 2;
            }
            this.CurrentHP = this.HP;
        }

        public virtual int Hit(Character c, int dice)
        {
            int DamageDone;
                
            if (this.Strenght <= 5)
            {
                DamageDone = (dice - 1) + this.DamageSup - this.DefenseSup;
            }
            else if (this.Strenght <= 10)
            {
                DamageDone = dice + this.DamageSup - this.DefenseSup;
            }
            else if (this.Strenght <= 15)
            {
                DamageDone = (dice + 1) + this.DamageSup - this.DefenseSup;
            }
            else
            {
                DamageDone = (dice + 2) + this.DamageSup - this.DefenseSup;
            }
            c.CurrentHP -= DamageDone;
            return DamageDone;
        }

        public virtual void Move()
        {

        }
    }
}
