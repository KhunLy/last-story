﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Helmet : IEquipment, ICuir, IPosition
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Initial { get; set; }
        public int AttackDamage { get; set; }
        public int Defense { get; set; }
        public string Name { get; set; }
        public Boolean Get { get; set; }
        public int Cuir { get; set; }
        public Helmet(int x, int y)
        {
            this.Initial = "‼";
            this.AttackDamage = 1;
            this.Defense = 1;
            this.X = x;
            this.Y = y;
            this.Name = "Light Helmet";
            this.Get = true;
            this.Cuir = 2;
        }
    }
}
