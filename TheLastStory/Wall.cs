﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Wall : IPosition, IWall
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Initial { get; set; }


        public Wall(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.Initial = "█";
        }
    }
}
