﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Armor : IEquipment, IOr, ICuir, IPosition
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Initial { get; set; }
        public int AttackDamage { get; set; }
        public int Defense { get; set; }
        public string Name { get; set; }
        public Boolean Get { get; set; }
        public int Or { get; set; }
        public int Cuir { get; set; }
        public Armor(int x, int y)
        {
            this.Initial = "‼";
            this.AttackDamage = 0;
            this.Defense = 4;
            this.X = x;
            this.Y = y;
            this.Name = "Leather Armor";
            this.Get = true;
            this.Or = 2;
            this.Cuir = 3;
        }
    }
}
