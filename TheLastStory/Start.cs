﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using TheLastStory.models;

namespace TheLastStory.controller
{
    class Start
    {
        public ConsoleColor ColorForeGround { get; set; }

        public Start()
        {
            ColorForeGround = ConsoleColor.Red;
        }

        public void Display() //10 x spc : "          " 20 x spc : "                    "
        {
            System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            player.SoundLocation = "../../music/215-the-memory-of-last-war.wav";
            player.Load();
            player.PlayLooping();
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Clear();
            Console.WriteLine
            (
                "\n\n\n\n" +
                "                                             ┌───────────────────────────────────────────────────────────┐                                            " +
                "                                             │                                                           │                                            " +
                "                                             │                     █████ █   █ █████                     │                                            " +
                "                                             │                       █   █   █ █                         │                                            " +
                "                                             │                       █   █   █ █                         │                                            " +
                "                                             │                       █   █████ ████                      │                                            " +
                "                                             │                       █   █   █ █                         │                                            " +
                "                                             │                       █   █   █ █                         │                                            " +
                "                                             │                       █   █   █ █████                     │                                            " +
                "                                             │                                                           │                                            " +
                "                                             │                                                           │                                            " +
                "                                             │                                                           │                                            " +
                "                                             │  █     ▄███▄ ▄███  █████   ▄███  █████ ▄███▄ ████▄ █   █  │                                            " +
                "                                             │  █     █   █ █       █     █       █   █   █ █   █ █   █  │                                            " +
                "                                             │  █     █   █ █       █     █       █   █   █ █   █ ▀█ █▀  │                                            " +
                "                                             │  █     █████ ▀███▄   █     ▀███▄   █   █   █ ████▀  ▀█▀   │                                            " +
                "                                             │  █     █   █     █   █         █   █   █   █ █ █▄    █    │                                            " +
                "                                             │  █     █   █     █   █         █   █   █   █ █  █▄   █    │                                            " +
                "                                             │  █████ █   █  ███▀   █      ███▀   █   ▀███▀ █   █   █    │                                            " +
                "                                             │                                                           │                                            " +
                "                                             └───────────────────────────────────────────────────────────┘                                            "
            );
            Console.ForegroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(64, 50);
            Console.WriteLine("Developped by  Khun LY");
            Console.SetCursorPosition(43, 51);
            Console.WriteLine("Music & Sound effect by  TAITO - Lufia and the Forteress of Doom");
            Boolean flag = true;
            do
            {
                while (!Console.KeyAvailable)
                {
                    if (flag)
                    {
                        Console.ForegroundColor = ColorForeGround;
                        Console.SetCursorPosition(0, 40);
                        Console.WriteLine("                                                                      PRESS ENTER                                                                     ");
                    }
                    else
                    {
                        Console.SetCursorPosition(0, 40);
                        Console.WriteLine("                                                                                                                                                      ");
                        if (ColorForeGround == ConsoleColor.DarkGreen)
                        {
                            ColorForeGround = ConsoleColor.Red;
                        }
                        else
                        {
                            ColorForeGround = ConsoleColor.DarkGreen;
                        }
                    }
                    flag = !flag;
                    System.Threading.Thread.Sleep(300);
                }
            }
            while (Console.ReadKey(true).Key != ConsoleKey.Enter);
            MediaPlayer player2 = new MediaPlayer();
            player2.Open(new Uri("../../music/start.wav", UriKind.Relative));
            player2.Play(); 
            player.SoundLocation = "../../music/214-departure.wav";
            player.Load();
            player.Play();
            Menu1 menu = new Menu1();
            menu.Display();
        }
    }
}
