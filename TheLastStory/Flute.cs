﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Flute : Accessory
    {
        public Flute(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.Initial = "|";
            this.Get = true;
            this.Name = "flute";
        }
    }
}
