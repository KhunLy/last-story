﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Orc : Monster, IOr
    {
        public int Or { get; set; }
        public Orc(string name, int stamina, int strenght) : base(name, stamina, strenght + 1)
        {
            this.Initial = "O";
            this.Type = "Orc";
        }
    }
}
