﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Sword : IPosition, IEquipment, IOr
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Initial { get; set; }
        public int AttackDamage { get; set; }
        public int Defense { get; set; }
        public string Name { get; set; }
        public Boolean Get { get; set; }
        public int Or { get; set; }
        public Sword(int x, int y)
        {
            this.Initial = "‼";
            this.AttackDamage = 2;
            this.Defense = 0;
            this.X = x;
            this.Y = y;
            this.Name = "Sword";
            this.Get = true;
            this.Or = 5;
        }
    }

}
