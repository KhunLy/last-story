﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Wolf : Monster, ICuir
    {
        public int Cuir { get; set; }

        public Wolf(string name, int stamina, int strenght) : base(name, stamina, strenght)
        {
            this.Initial = "W";
            this.Type = "Wolf";
        }
    }
}
