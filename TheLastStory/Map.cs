﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    abstract class Map
    {
        public string Image { get; set; } //43x29
        public List<IPosition> PositionList { get; set; }
        public Hero Hero { get; set; }
        public int NbWall { get; set; }

        public Map(Hero hero)
        {
            this.PositionList = new List<IPosition>();
            this.PositionList.Add(hero);
            this.Hero = hero;
        }

        public void GenerateMonsters(int monsterNb)
        {
            Dice d6 = new Dice(6);
            Dice d3 = new Dice(3);
            Dice boardSize = new Dice(13);
            int monster;
            int strength;
            int stamina;
            int posX;
            int posY;
            int cpt = 1;
            Boolean flag = true;
            while (this.PositionList.Count < monsterNb + 1 + NbWall || cpt >= 1000 )
            {
                cpt++;
                monster = d3.ThrowDice();
                strength = d6.Roll4Dices();
                stamina = d6.Roll4Dices();
                posX = boardSize.ThrowDice();
                posY = boardSize.ThrowDice();
                flag = true;
                Monster m;
                foreach (IPosition p in this.PositionList)
                {
                    if (p is Character)
                    {
                        if ((p.X == posX && Math.Abs(p.Y - posY) <= 2) || (p.Y == posY && Math.Abs(p.X - posX) <= 2))
                        {
                            flag = false;
                        }
                        else if ((Math.Abs(p.X - posX) == 1 && Math.Abs(p.Y - posY) <= 1) || (Math.Abs(p.Y - posY) == 1 && Math.Abs(p.X - posX) <= 1))
                        {
                            flag = false;
                        }
                    }
                    else if (p is Wall || p is Exit)
                    {
                        if (p.X == posX && p.Y == posY)
                        {
                            flag = false;
                        }
                    }   
                }
                if (flag)
                {
                    if (monster == 1)
                    {
                        m = new Wolf("Wolf", stamina, strength);
                        m.X = posX;
                        m.Y = posY;
                    }
                    else if (monster == 2)
                    {
                        m = new Orc("Orc", stamina, strength);
                        m.X = posX;
                        m.Y = posY;
                    }
                    else
                    {
                        m = new Dragon("Dragon", stamina, strength);
                        m.X = posX;
                        m.Y = posY;
                    }
                    this.PositionList.Add(m);
                }
            }
        }

        public void Display()
        {
            for (int i = 0; i<29; i++)
            {
                Console.SetCursorPosition(8, 12 + i);
                for (int j = 0; j < 43; j++)
                {
                    Console.Write(Image[((43* i) + j)]);
                }
            }
            foreach (var p in PositionList)
            {
                if (p is Hero || p is Accessory || p is INPC)
                {
                    if (p is Accessory)
                    {
                        if (!((Accessory)p).Get)
                        {
                            continue;
                        }
                    }
                    Console.SetCursorPosition((p.X) * 3 + 8, (p.Y) * 2 + 12);
                    Console.Write(p.Initial);
                }
            }
        }
    }
}
