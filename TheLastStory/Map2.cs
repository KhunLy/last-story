﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheLastStory.models;

namespace TheLastStory.controller
{
    class Map2 : Map
    {
        public Map2(Hero hero) : base(hero)
        {
            this.NbWall = 0;
            Image =
                "████████████████████   ████████████████████" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█████████████                             █" +
                "            █                             █" +
                "            █                             █" +
                "            █                             █" +
                "████████████████████████████              █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█           █                             █" +
                "█           █                             █" +
                "█           █                             █" +
                "█           █                             █" +
                "█           ████████████████              █" +
                "█                          █              █" +
                "█                          █              █" +
                "█                          █              █" +
                "████████████████████   ████████████████████";
            //Map1 m = new Map1(hero);
            //this.PositionList.Add(new Exit(5, 5, m));

            //this.NbWall = 1;
            for (int i = 0; i < 15; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    if (i == 0 && j != 7)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                    else if (i == 14 && j != 7)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                    else if (i != 7 && j == 0)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                    else if (i != 7 && j == 14)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                }
            }
            this.PositionList.Add(new Wall(14, 7));
            this.NbWall += 1;
            this.PositionList.Add(new Wall(1, 6));
            this.PositionList.Add(new Wall(2, 6));
            this.PositionList.Add(new Wall(3, 6));
            this.PositionList.Add(new Wall(4, 6));
            this.PositionList.Add(new Wall(4, 7));
            this.PositionList.Add(new Wall(7, 12));
            this.PositionList.Add(new Wall(8, 12));
            this.PositionList.Add(new Wall(9, 12));
            this.PositionList.Add(new Wall(9, 13));
            this.PositionList.Add(new Wall(6, 12));
            this.PositionList.Add(new Wall(5, 12));
            this.PositionList.Add(new Wall(4, 12));
            this.PositionList.Add(new Wall(4, 11));
            this.PositionList.Add(new Wall(4, 10));
            this.PositionList.Add(new Wall(1, 8));
            this.PositionList.Add(new Wall(2, 8));
            this.PositionList.Add(new Wall(3, 8));
            this.PositionList.Add(new Wall(4, 8));
            this.PositionList.Add(new Wall(5, 8));
            this.PositionList.Add(new Wall(6, 8));
            this.PositionList.Add(new Wall(7, 8));
            this.PositionList.Add(new Wall(8, 8));
            this.PositionList.Add(new Wall(9, 8));
            Map outMap = new Map1(this.Hero);
            this.PositionList.Add(new Exit(7, 0, outMap));
            outMap.PositionList.Add(new Exit(7, 14, this));
            outMap.NbWall += 1;
            this.NbWall += 24;
            Map tempMap = outMap;
            foreach (IPosition p in tempMap.PositionList)
            {
                if (p is Exit && p.X == 0 && p.Y == 7)
                {
                    tempMap = ((Exit)p).OutMap;
                    foreach (IPosition p2 in tempMap.PositionList)
                    {
                        if (p2 is Exit && p2.X == 7 && p2.Y == 14)
                        {
                            tempMap = ((Exit)p2).OutMap;
                        }
                    }
                }
            }
            this.PositionList.Add(new Exit(0, 7, tempMap));
            tempMap.PositionList.Add(new Exit(14, 7, this));
            tempMap.NbWall += 1;
            this.NbWall += 1;
            Flute flute = new Flute(3, 7);
            this.PositionList.Add(flute);
            this.NbWall += 1;
        }
    }
}
