﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheLastStory.models;

namespace TheLastStory.controller
{
    class GameTestMode
    {
        public Dice SixFaces { get; set; }
        public Dice FourFaces { get; set; } 
        public Hero Hero { get; set; }
        public Map Map { get; set; }
        public Background Bg { get; set; }
        public List<IPosition> PList { get; set; }
        public Boolean GoToWall { get; set; }
        public int NbMonsters { get; set; }
        public System.Windows.Media.MediaPlayer MP { get; set; }

        public GameTestMode(int hero, string name, int nbMonsters)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Clear();
            this.SixFaces = new Dice(6);
            this.FourFaces = new Dice(4);
            int strength = SixFaces.Roll4Dices();
            int stamina = SixFaces.Roll4Dices();
            if (hero == 1)
                this.Hero = new Human(name, stamina, strength);
            else if (hero == 2)
                this.Hero = new Dwarf(name, stamina, strength);
            this.Map = new Map2(this.Hero);
            this.NbMonsters = nbMonsters;
            this.Map.GenerateMonsters(nbMonsters);
            this.PList = this.Map.PositionList;
            this.Bg = new Background();
            this.MP = new System.Windows.Media.MediaPlayer();
        }

        public void Start()
        {
            this.Bg.Display();
            this.Map.Display();
            this.Hero.Display();
            System.Media.SoundPlayer player0 = new System.Media.SoundPlayer();
            player0.SoundLocation = "../../music/03-last-battle.wav";
            player0.Load();
            player0.Play();
            ConsoleKey k = Console.ReadKey(true).Key;
            while (k != ConsoleKey.Escape)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                GoToWall = false;
                foreach (IPosition position in PList)
                {
                    if (position is IWall)
                    {
                        if (this.Hero.GoToWall(k, (IWall)position))
                        {
                            GoToWall = true;
                        }
                    }
                }
                foreach (IPosition position in PList)
                {
                    if (position is Exit)
                    {
                        if (this.Hero.On(position))
                        {
                            Console.SetCursorPosition((this.Hero.X) * 3 + 8, (this.Hero.Y) * 2 + 12);
                            if (this.Hero.X == 7)
                            {
                                if (this.Hero.Y == 0)
                                {
                                    this.Hero.Y = 13;
                                }
                                else if (this.Hero.Y == 14)
                                {
                                    this.Hero.Y = 1;
                                }
                            }
                            else if (this.Hero.Y == 7)
                            {
                                if (this.Hero.X == 0)
                                {
                                    this.Hero.X = 13;
                                }
                                else if (this.Hero.X == 14)
                                {
                                    this.Hero.X = 1;
                                }
                            }
                            Console.Write(" ");
                            this.Map = ((Exit)position).OutMap;
                            this.Map.GenerateMonsters(NbMonsters);
                            this.PList = this.Map.PositionList;
                            this.Map.Display();
                            k = Console.ReadKey().Key;
                        }
                    }
                }
                if (!GoToWall)
                {
                    while (!(this.Hero.Move(k)))
                    {
                        break;
                    }
                    Lib.GameInfo("");
                    Boolean flag = true;
                    foreach (IPosition ch in PList)
                    {
                        if (ch is Accessory)
                        {
                            if (this.Hero.On(ch))
                            {
                                this.Hero.AddAccessory((Accessory)ch);
                                if (((Accessory)ch).Get)
                                {
                                    MP.Open(new Uri("../../music/ItemFanfare.wav", UriKind.Relative));
                                    MP.Play();
                                    Lib.GameInfo("You Find a " + ((Accessory)ch).Name);

                                }
                                ((Accessory)ch).Get = false;
                                this.Hero.Display();

                            }
                        }
                        if (ch is INPC)
                        {
                            INPC npc = (INPC)ch; 
                            if (this.Hero.NextTo(npc))
                            {
                                Lib.GameInfo($"{npc.Name} : {npc.Sentence}");
                                if (npc is Merchant)
                                {
                                    List<ConsoleColor> colorList = new List<ConsoleColor>() { ConsoleColor.Red , ConsoleColor.DarkGreen};
                                    int color = 0;
                                    int i = 0;
                                    Merchant merch = (Merchant)npc;
                                    Lib.Action1("BUY");
                                    Lib.Action2("SELL");
                                    Lib.Action3("RESTORE");
                                    Lib.Action4("QUIT");
                                    ConsoleKeyInfo cki;
                                    do
                                    {
                                        while (!Console.KeyAvailable)
                                        {
                                            Console.ForegroundColor = colorList[color];
                                            switch (i)
                                            {
                                                case 0:
                                                    Lib.Action1("BUY");
                                                    break;
                                                case 1:
                                                    Lib.Action2("SELL");
                                                    break;
                                                case 2:
                                                    Lib.Action3("RESTORE");
                                                    break;
                                                default:
                                                    Lib.Action4("QUIT");
                                                    break;
                                            }
                                            color = (color + 1) % 2;
                                            System.Threading.Thread.Sleep(300);

                                        }
                                        cki = Console.ReadKey(true);
                                        if (cki.Key == ConsoleKey.LeftArrow)
                                        {
                                            Console.ForegroundColor = ConsoleColor.Blue;
                                            switch (i)
                                            {
                                                case 0:
                                                    Lib.Action1("BUY");
                                                    break;
                                                case 1:
                                                    Lib.Action2("SELL");
                                                    break;
                                                case 2:
                                                    Lib.Action3("RESTORE");
                                                    break;
                                                default:
                                                    Lib.Action4("QUIT");
                                                    break;
                                            }
                                            i = (i + 3) % 4;
                                        }
                                        else if (cki.Key == ConsoleKey.RightArrow)
                                        {
                                            Console.ForegroundColor = ConsoleColor.Blue;
                                            switch (i)
                                            {
                                                case 0:
                                                    Lib.Action1("BUY");
                                                    break;
                                                case 1:
                                                    Lib.Action2("SELL");
                                                    break;
                                                case 2:
                                                    Lib.Action3("RESTORE");
                                                    break;
                                                default:
                                                    Lib.Action4("QUIT");
                                                    break;
                                            }
                                            i = (i + 1) % 4;
                                        }
                                    }
                                    while (cki.Key != ConsoleKey.Spacebar);
                                    Lib.ActionClear();
                                    if (i == 0)
                                    {
                                        i = 0;
                                        Lib.Action1($"{merch.Inventory[0].Name}");
                                        Lib.Action2($"{merch.Inventory[1].Name}");
                                        Lib.Action3($"{merch.Inventory[2].Name}");
                                        Lib.Action4($"{merch.Inventory[3].Name}");
                                        do
                                        {
                                            Lib.GameInfo(string.Format("PRICE : {0} Gold ", merch.Inventory[i] is IOr ? ((IOr)merch.Inventory[i]).Or : 0) + string.Format("{0} Leather", merch.Inventory[i] is ICuir ? ((ICuir)merch.Inventory[i]).Cuir : 0) + String.Format(" | STATS : {0} AD - {1} DEF", merch.Inventory[i].AttackDamage, merch.Inventory[i].Defense));

                                            while (!Console.KeyAvailable)
                                            {
                                                Console.ForegroundColor = colorList[color];
                                                switch (i)
                                                {
                                                    case 0:
                                                        Lib.Action1($"{merch.Inventory[0].Name}");
                                                        break;
                                                    case 1:
                                                        Lib.Action2($"{merch.Inventory[1].Name}");
                                                        break;
                                                    case 2:
                                                        Lib.Action3($"{merch.Inventory[2].Name}");
                                                        break;
                                                    default:
                                                        Lib.Action4($"{merch.Inventory[3].Name}");
                                                        break;
                                                }
                                                color = (color + 1) % 2;
                                                System.Threading.Thread.Sleep(300);

                                            }
                                            cki = Console.ReadKey(true);
                                            if (cki.Key == ConsoleKey.LeftArrow)
                                            {
                                                Console.ForegroundColor = ConsoleColor.Blue;
                                                switch (i)
                                                {
                                                    case 0:
                                                        Lib.Action1($"{merch.Inventory[0].Name}");
                                                        break;
                                                    case 1:
                                                        Lib.Action2($"{merch.Inventory[1].Name}");
                                                        break;
                                                    case 2:
                                                        Lib.Action3($"{merch.Inventory[2].Name}");
                                                        break;
                                                    default:
                                                        Lib.Action4($"{merch.Inventory[3].Name}");
                                                        break;
                                                }
                                                i = (i + 3) % 4;
                                            }
                                            else if (cki.Key == ConsoleKey.RightArrow)
                                            {
                                                Console.ForegroundColor = ConsoleColor.Blue;
                                                switch (i)
                                                {
                                                    case 0:
                                                        Lib.Action1($"{merch.Inventory[0].Name}");
                                                        break;
                                                    case 1:
                                                        Lib.Action2($"{merch.Inventory[1].Name}");
                                                        break;
                                                    case 2:
                                                        Lib.Action3($"{merch.Inventory[2].Name}");
                                                        break;
                                                    default:
                                                        Lib.Action4($"{merch.Inventory[3].Name}");
                                                        break;
                                                }
                                                i = (i + 1) % 4;
                                            }
                                        }
                                        while (cki.Key != ConsoleKey.Spacebar);
                                        bool buyingFlag = true;
                                        if (merch.Inventory[i] is IOr)
                                        {
                                            if(((IOr)merch.Inventory[i]).Or > this.Hero.Wallet)
                                            {
                                                buyingFlag = false;
                                            }
                                        }
                                        if (merch.Inventory[i] is ICuir)
                                        {
                                            if (((ICuir)merch.Inventory[i]).Cuir > this.Hero.Wallet)
                                            {
                                                buyingFlag = false;
                                            }
                                        }
                                        if (buyingFlag)
                                        {
                                            if (this.Hero.Weapon == null )
                                            {
                                                if (merch.Inventory[i] is IOr)
                                                {
                                                    this.Hero.Wallet -= ((IOr)merch.Inventory[i]).Or;
                                                }
                                                if (merch.Inventory[i] is ICuir)
                                                {
                                                    this.Hero.Bag -= ((ICuir)merch.Inventory[i]).Cuir;
                                                }
                                                Lib.GameInfo($"{npc.Name} :  Thank You !!");
                                                this.Hero.Equip(merch.Inventory[i]);
                                                Console.ReadKey();
                                            }
                                            else
                                            {
                                                Lib.GameInfo($"{npc.Name} : You already have a weapon !!");
                                                Console.ReadKey();
                                            }
                                        }
                                        else
                                        {
                                            Lib.GameInfo($"{npc.Name} :  You don't have enough to buy this item !!");
                                            Console.ReadKey();
                                        }
                                        Lib.ActionClear();
                                    }  
                                }
                                Lib.GameInfo($"{npc.Name} : GOOD BYE !");
                            }
                        }
                        if (ch is Monster)
                        {
                            Monster m = (Monster)ch;
                            if (this.Hero.NextTo(m))
                            {
                                flag = false;
                                m.Display();
                                Console.SetCursorPosition((m.X) * 3 + 8, (m.Y) * 2 + 12);
                                if (m.Alive)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.Write(m.Initial);
                                    Console.ForegroundColor = ConsoleColor.Blue;
                                    Fight f = new Fight(this.Hero, m, FourFaces);
                                    f.Start();
                                    player0.Play();
                                    this.Hero.Restore();
                                    Lib.DisplayCurrentHP(this.Hero);
                                }
                                if (!m.Alive)
                                {
                                    Console.SetCursorPosition((m.X) * 3 + 8, (m.Y) * 2 + 12);
                                    if (m.Loot)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Cyan;
                                        Console.Write("$");
                                        Console.ForegroundColor = ConsoleColor.Blue;

                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.DarkGray;
                                        Console.Write(m.Initial);
                                        Console.ForegroundColor = ConsoleColor.Blue;
                                    }
                                }
                            }
                            if (flag)
                            {
                                Monster.UnDisplay();
                            }
                            if (this.Hero.On(m))
                            {
                                Console.SetCursorPosition((this.Hero.X) * 3 + 8, (this.Hero.Y) * 2 + 12);
                                Console.Write("☺");
                                if (!m.Alive)
                                {
                                    if (m.Loot)
                                    {
                                        this.Hero.Loot(m);
                                        if (m is IOr)
                                        {
                                            Lib.GameInfo("You Find " + ((IOr)m).Or.ToString() + " coin(s) of gold !!");
                                        }
                                        if (m is ICuir)
                                        {
                                            if (!(m is IOr))
                                            {
                                                Lib.GameInfo("You Find " + ((ICuir)m).Cuir.ToString() + " leather(s) !!");
                                            }

                                            else
                                            {
                                                System.Threading.Thread.Sleep(1000);
                                                Lib.GameInfo("You Find " + ((ICuir)m).Cuir.ToString() + " leather(s) !!");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    k = Console.ReadKey(true).Key;
                }
                else
                {
                    k = Console.ReadKey(true).Key;
                }
            }
        }
    }
}
