﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TheLastStory.controller
{
    class Menu1
    {
        public ConsoleKeyInfo cki { get; set; }
        public ConsoleColor ColorForeGround { get; set; }
        public MediaPlayer player { get; set; }

        public Menu1()
        {
            this.ColorForeGround = ConsoleColor.Red;
            this.player = new MediaPlayer();
            Console.SetCursorPosition(0, 40);
            Console.WriteLine("                                                                                                                                                      ");
        }

        public void Display()
        {
            Boolean flag = true;
            do
            {
                while (!Console.KeyAvailable)
                {
                    if (flag)
                    {
                        Console.ForegroundColor = ColorForeGround;
                        Console.SetCursorPosition(71, 39);
                        Console.WriteLine("TEST MODE");
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.SetCursorPosition(72, 43);
                        Console.WriteLine("OPTIONS");
                        Console.SetCursorPosition(70, 41);
                        Console.WriteLine("BEGIN STORY");
                    }
                    else
                    {
                        Console.SetCursorPosition(71, 39);
                        Console.WriteLine("              ");
                        if (ColorForeGround == ConsoleColor.DarkGreen)
                        {
                            ColorForeGround = ConsoleColor.Red;
                        }
                        else
                        {
                            ColorForeGround = ConsoleColor.DarkGreen;
                        }
                    }
                    flag = !flag;
                    System.Threading.Thread.Sleep(300);
                }
                cki = Console.ReadKey(true);
            }
            while (cki.Key != ConsoleKey.DownArrow && cki.Key != ConsoleKey.UpArrow && cki.Key != ConsoleKey.Enter);
            if (cki.Key == ConsoleKey.UpArrow)
            {
                player.Open(new Uri("../../music/select.wav", UriKind.Relative));
                player.Play();
                Menu2 menu2 = new Menu2();
                menu2.Display();
            }
            else if (cki.Key == ConsoleKey.DownArrow)
            {
                player.Open(new Uri("../../music/select.wav", UriKind.Relative));
                player.Play();
                Menu3 menu3 = new Menu3();
                menu3.Display();
            }
            else
            {
                player.Open(new Uri("../../music/start.wav", UriKind.Relative));
                player.Play();
                MenuTest1 menu = new MenuTest1();
                menu.Display();
            }
        }

        public void DisplayTop()
        {
            {
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Clear();
                Console.WriteLine
                (
                    "\n\n\n\n" +
                    "                                             ┌───────────────────────────────────────────────────────────┐                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │                     █████ █   █ █████                     │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █████ ████                      │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █   █ █████                     │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │  █     ▄███▄ ▄███  █████   ▄███  █████ ▄███▄ ████▄ █   █  │                                            " +
                    "                                             │  █     █   █ █       █     █       █   █   █ █   █ █   █  │                                            " +
                    "                                             │  █     █   █ █       █     █       █   █   █ █   █ ▀█ █▀  │                                            " +
                    "                                             │  █     █████ ▀███▄   █     ▀███▄   █   █   █ ████▀  ▀█▀   │                                            " +
                    "                                             │  █     █   █     █   █         █   █   █   █ █ █▄    █    │                                            " +
                    "                                             │  █     █   █     █   █         █   █   █   █ █  █▄   █    │                                            " +
                    "                                             │  █████ █   █  ███▀   █      ███▀   █   ▀███▀ █   █   █    │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             └───────────────────────────────────────────────────────────┘                                            "
                );
            }
        }
    }
}
