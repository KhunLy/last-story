﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TheLastStory.controller
{
    class Menu2
    {
        public ConsoleKeyInfo cki { get; set; }
        public ConsoleColor ColorForeGround { get; set; }

        public Menu2()
        {
            this.ColorForeGround = ConsoleColor.Red;
            Console.SetCursorPosition(0, 40);
            Console.WriteLine("                                                                                                                                                      ");
    }

        public void Display()
        {
            Boolean flag = true;
            do
            {
                while (!Console.KeyAvailable)
                {
                    if (flag)
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.SetCursorPosition(71, 39);
                        Console.WriteLine("TEST MODE");
                        Console.ForegroundColor = ColorForeGround;
                        Console.SetCursorPosition(72, 43);
                        Console.WriteLine("OPTIONS");
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.SetCursorPosition(70, 41);
                        Console.WriteLine("BEGIN STORY");
                    }
                    else
                    {
                        Console.SetCursorPosition(72, 43);
                        Console.WriteLine("          ");
                        if (ColorForeGround == ConsoleColor.DarkGreen)
                        {
                            ColorForeGround = ConsoleColor.Red;
                        }
                        else
                        {
                            ColorForeGround = ConsoleColor.DarkGreen;
                        }
                    }
                    flag = !flag;
                    System.Threading.Thread.Sleep(300);
                }
                cki = Console.ReadKey(true);
            }
            while (cki.Key != ConsoleKey.DownArrow && cki.Key != ConsoleKey.UpArrow && cki.Key != ConsoleKey.Enter);
            MediaPlayer player = new MediaPlayer();
            player.Open(new Uri("../../music/select.wav", UriKind.Relative));
            player.Play();
            if (cki.Key == ConsoleKey.UpArrow)
            {
                Menu3 menu3 = new Menu3();
                menu3.Display();
            }
            else if (cki.Key == ConsoleKey.DownArrow)
            {
                Menu1 menu1 = new Menu1();
                menu1.Display();
            }
            else if (cki.Key == ConsoleKey.Enter)
            {
                Console.WriteLine("ca marche vers options");
                Console.ReadKey();
            }
        }

        public void DisplayTop()
        {
            {
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Clear();
                Console.WriteLine
                (
                    "\n\n\n\n" +
                    "                                             ┌───────────────────────────────────────────────────────────┐                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │                     █████ █   █ █████                     │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █████ ████                      │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █   █ █████                     │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │  █     ▄███▄ ▄███  █████   ▄███  █████ ▄███▄ ████▄ █   █  │                                            " +
                    "                                             │  █     █   █ █       █     █       █   █   █ █   █ █   █  │                                            " +
                    "                                             │  █     █   █ █       █     █       █   █   █ █   █ ▀█ █▀  │                                            " +
                    "                                             │  █     █████ ▀███▄   █     ▀███▄   █   █   █ ████▀  ▀█▀   │                                            " +
                    "                                             │  █     █   █     █   █         █   █   █   █ █ █▄    █    │                                            " +
                    "                                             │  █     █   █     █   █         █   █   █   █ █  █▄   █    │                                            " +
                    "                                             │  █████ █   █  ███▀   █      ███▀   █   ▀███▀ █   █   █    │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             └───────────────────────────────────────────────────────────┘                                            "
                );
            }
        }
    }
}
