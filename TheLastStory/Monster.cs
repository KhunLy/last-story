﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    abstract class Monster : Character
    {
        public Boolean Alive { get; set; }
        public Boolean Loot { get; set; }

        public Monster (string name, int stamina, int strenght) : base(name, stamina, strenght)
        {
            Alive = true;
            Loot = true;
            Dice d4 = new Dice(4);
            Dice d6 = new Dice(6);
            if (this is IOr)
            {
                ((IOr)this).Or = d6.ThrowDice();
            }
            if (this is ICuir)
            {
                ((ICuir)this).Cuir = d4.ThrowDice();
            }
        }

        public void Display()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.SetCursorPosition(111, 12);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(111, 12);
            Console.WriteLine(this.Name);
            Console.SetCursorPosition(111, 13);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(111, 13);
            Console.WriteLine(this.Type);
            Console.SetCursorPosition(115, 14);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(115, 14);
            Console.WriteLine(this.Strenght);
            Console.SetCursorPosition(114, 15);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(114, 15);
            Console.WriteLine(this.Stamina);
            Lib.DisplayCurrentHP(this);
        }
        public static void UnDisplay()
        {
            Console.SetCursorPosition(111, 12);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(111, 13);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(115, 14);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(114, 15);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(109, 16);
            Console.WriteLine("                    ");
        }
    }
}
