﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class FluteSecret : Secret
    {
        public FluteSecret()
        {
            this.AccessoryName = "Flute";
        }
    }
}
