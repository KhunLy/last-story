﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    interface ICharacter
    {
        int Hit(Character C, int dice);
        void Move();
    }
}
