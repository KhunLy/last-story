﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Dwarf : Hero
    {
        public Dwarf(string name, int stamina, int strenght) : base (name, stamina + 2, strenght)
        {
            this.Type = "Dwarf";
        }
    }
}
