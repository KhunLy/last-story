﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    abstract class Accessory : IPosition
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Initial { get; set; }
        public bool Get { get; set; }
        public string Name { get; set; }
    }
}