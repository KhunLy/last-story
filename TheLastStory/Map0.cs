﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheLastStory.models;

namespace TheLastStory.controller
{
    class Map0 : Map
    {
        public Map0(Hero hero) : base(hero)
        {
            this.NbWall = 0;
            Image =
                "████████████████████   ████████████████████" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "                                           " +
                "                                           " +
                "                                           " +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "████████████████████   ████████████████████";
            //Map1 m = new Map1(hero);
            //this.PositionList.Add(new Exit(5, 5, m));

            //this.NbWall = 1;
            for (int i = 0; i < 15; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    if (i == 0 && j != 7)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                    else if (i == 14 && j != 7)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                    else if (i != 7 && j == 0)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                    else if (i != 7 && j == 14)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                }
            }
        }
    }
}
