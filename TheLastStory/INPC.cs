﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    interface INPC : IPosition, IWall
    {
        string Name { get; set; }
        string Initial { get; set; }
        string Sentence { get; set; }
    }
}
