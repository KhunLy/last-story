﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TheLastStory.controller
{
    class MenuTest1
    {
        public ConsoleKeyInfo cki { get; set; }
        public ConsoleColor ColorForeGround { get; set; }

        public MenuTest1()
        {
            this.ColorForeGround = ConsoleColor.Red;
            Console.SetCursorPosition(0, 40);
            Console.WriteLine("                                                                                                                                                      ");
        }

        public void Display()
        {
            Boolean flag = true;
            do
            {
                while (!Console.KeyAvailable)
                {
                    if (flag)
                    {
                        Console.ForegroundColor = ColorForeGround;
                        Console.SetCursorPosition(68, 39);
                        Console.WriteLine("INVICIBLE MODE");
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.SetCursorPosition(72, 43);
                        Console.WriteLine("       ");
                        Console.SetCursorPosition(68, 41);
                        Console.WriteLine("NO ENEMY  MODE");
                    }
                    else
                    {
                        Console.SetCursorPosition(68, 39);
                        Console.WriteLine("                  ");
                        if (ColorForeGround == ConsoleColor.DarkGreen)
                        {
                            ColorForeGround = ConsoleColor.Red;
                        }
                        else
                        {
                            ColorForeGround = ConsoleColor.DarkGreen;
                        }
                    }
                    flag = !flag;
                    System.Threading.Thread.Sleep(300);
                }
                cki = Console.ReadKey(true);
            }
            while (cki.Key != ConsoleKey.DownArrow && cki.Key != ConsoleKey.UpArrow && cki.Key != ConsoleKey.Enter);
            MediaPlayer player = new MediaPlayer();
            player.Open(new Uri("../../music/select.wav", UriKind.Relative));
            player.Play();
            if (cki.Key == ConsoleKey.UpArrow || cki.Key == ConsoleKey.DownArrow)
            {
                MenuTest2 menu = new MenuTest2();
                menu.Display();
            }
            else
            {
                GameTestMode g = new GameTestMode(1, "Khun", 10);
                g.Start();
            }
        }

        public void DisplayTop()
        {
            {
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Clear();
                Console.WriteLine
                (
                    "\n\n\n\n" +
                    "                                             ┌───────────────────────────────────────────────────────────┐                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │                     █████ █   █ █████                     │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █████ ████                      │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █   █ █                         │                                            " +
                    "                                             │                       █   █   █ █████                     │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             │  █     ▄███▄ ▄███  █████   ▄███  █████ ▄███▄ ████▄ █   █  │                                            " +
                    "                                             │  █     █   █ █       █     █       █   █   █ █   █ █   █  │                                            " +
                    "                                             │  █     █   █ █       █     █       █   █   █ █   █ ▀█ █▀  │                                            " +
                    "                                             │  █     █████ ▀███▄   █     ▀███▄   █   █   █ ████▀  ▀█▀   │                                            " +
                    "                                             │  █     █   █     █   █         █   █   █   █ █ █▄    █    │                                            " +
                    "                                             │  █     █   █     █   █         █   █   █   █ █  █▄   █    │                                            " +
                    "                                             │  █████ █   █  ███▀   █      ███▀   █   ▀███▀ █   █   █    │                                            " +
                    "                                             │                                                           │                                            " +
                    "                                             └───────────────────────────────────────────────────────────┘                                            "
                );
            }
        }
    }
}

