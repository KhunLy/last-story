﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Human : Hero
    {
        public Human(string name, int stamina, int strenght) : base(name, stamina + 1, strenght + 1)
        {
            this.Type = "Human";
        }
    }
}
