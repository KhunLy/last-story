﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheLastStory.models;
using System.Windows.Media;

namespace TheLastStory.controller
{
    class Game
    {
        public MediaPlayer MP { get; set; }
        public Dice SixFaces { get; set; }
        public Dice FourFaces { get; set; }
        public Hero Hero { get; set; }
        public Map Map { get; set; }
        public Background Bg { get; set; }
        public List<IPosition> PList { get; set; }
        public Boolean GoToWall { get; set; }
        public int NbMonsters { get; set; }

        public Game(int hero, string name, int nbMonsters)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Clear();
            this.SixFaces = new Dice(6);
            this.FourFaces = new Dice(4);
            int strength = SixFaces.Roll4Dices();
            int stamina = SixFaces.Roll4Dices();
            if (hero == 1)
                this.Hero = new Human(name, stamina, strength);
            else if (hero == 2)
                this.Hero = new Dwarf(name, stamina, strength);
            this.Map = new Map2(this.Hero);
            this.NbMonsters = nbMonsters;
            this.Map.GenerateMonsters(nbMonsters);
            this.PList = this.Map.PositionList;
            this.MP = new MediaPlayer();
            this.Bg = new Background();
        }

        public void Start()
        {
            this.Bg.Display();
            this.Map.Display();
            this.Hero.Display();
            System.Media.SoundPlayer player0 = new System.Media.SoundPlayer();
            player0.SoundLocation = "../../music/03-last-battle.wav";
            player0.Load();
            player0.Play();
            ConsoleKey k = Console.ReadKey(true).Key;
            while (k != ConsoleKey.Escape)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                GoToWall = false;
                foreach (IPosition position in PList)
                {
                    if (position is IWall)
                    {
                        if (this.Hero.GoToWall(k, (IWall)position))
                        {
                            GoToWall = true;
                        }
                    }
                }
                foreach (IPosition position in PList)
                {
                    if (position is Exit)
                    {
                        if (this.Hero.On(position))
                        {
                            Console.SetCursorPosition((this.Hero.X) * 3 + 8, (this.Hero.Y) * 2 + 12);
                            if (this.Hero.X == 7)
                            {
                                if (this.Hero.Y == 0)
                                {
                                    this.Hero.Y = 13;
                                }
                                else if (this.Hero.Y == 14)
                                {
                                    this.Hero.Y = 1;
                                }
                            }
                            else if (this.Hero.Y == 7)
                            {
                                if (this.Hero.X == 0)
                                {
                                    this.Hero.X = 13;
                                }
                                else if (this.Hero.X == 14)
                                {
                                    this.Hero.X = 1;
                                }
                            }
                            Console.Write(" ");
                            this.Map = ((Exit)position).OutMap;
                            this.Map.GenerateMonsters(NbMonsters);
                            this.PList = this.Map.PositionList;
                            this.Map.Display();
                        }
                    }
                }
                if (!GoToWall)
                {
                    while (!(this.Hero.Move(k)))
                    {
                        break;
                    }
                    Lib.GameInfo("");
                    Boolean flag = true;
                    foreach (IPosition ch in PList)
                    {
                        if (ch is IEquipment)
                        {
                            if (this.Hero.On(ch))
                            {
                                this.Hero.Weapon = (IEquipment)ch;
                                this.Hero.Strenght = this.Hero.InitialStrenght + ((IEquipment)ch).AttackDamage;
                                if (((IEquipment)ch).Get)
                                {
                                    MP.Open(new Uri("../../music/ItemFanfare.wav", UriKind.Relative));
                                    MP.Play();
                                    Lib.GameInfo("You Find a " + ((IEquipment)ch).Name);

                                }
                                ((IEquipment)ch).Get = false;
                                this.Hero.Display();

                            }
                        }
                        if (ch is Monster)
                        {
                            Monster m = (Monster)ch;
                            if (this.Hero.NextTo(m))
                            {
                                flag = false;
                                m.Display();
                                Console.SetCursorPosition((m.X) * 3 + 8, (m.Y) * 2 + 12);
                                if (m.Alive)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.Write(m.Initial);
                                    Console.ForegroundColor = ConsoleColor.Blue;
                                    Fight f = new Fight(this.Hero, m, FourFaces);
                                    f.Start();
                                    if (this.Hero.CurrentHP <= 0)
                                    {
                                        GameOver go = new GameOver();
                                        go.Display();
                                    }
                                    player0.Play();
                                    this.Hero.Restore();
                                    Lib.DisplayCurrentHP(this.Hero);
                                }
                                if (!m.Alive)
                                {
                                    Console.SetCursorPosition((m.X) * 3 + 8, (m.Y) * 2 + 12);
                                    if (m.Loot)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Cyan;
                                        Console.Write("$");
                                        Console.ForegroundColor = ConsoleColor.Blue;

                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.DarkGray;
                                        Console.Write(m.Initial);
                                        Console.ForegroundColor = ConsoleColor.Blue;
                                    }
                                }
                            }
                            if (flag)
                            {
                                Monster.UnDisplay();
                            }
                            if (this.Hero.On(m))
                            {
                                Console.SetCursorPosition((this.Hero.X) * 3 + 8, (this.Hero.Y) * 2 + 12);
                                Console.Write("☺");
                                if (!m.Alive)
                                {
                                    if (m.Loot)
                                    {
                                        this.Hero.Loot(m);
                                        if (m is IOr)
                                        {
                                            Lib.GameInfo("You Find " + ((IOr)m).Or.ToString() + " coin(s) of gold !!");
                                        }
                                        if (m is ICuir)
                                        {
                                            if (!(m is IOr))
                                            {
                                                Lib.GameInfo("You Find " + ((ICuir)m).Cuir.ToString() + " leather(s) !!");
                                            }

                                            else
                                            {
                                                System.Threading.Thread.Sleep(1000);
                                                Lib.GameInfo("You Find " + ((ICuir)m).Cuir.ToString() + " leather(s) !!");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    k = Console.ReadKey(true).Key;
                }
                else
                {
                    k = Console.ReadKey(true).Key;
                }
            }
        }
    }
}
