﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheLastStory.models;

namespace TheLastStory.controller
{
    class Map1 : Map
    {
        public Map1(Hero hero) : base(hero)
        {
            this.NbWall = 0;
            Image =
                "████████████████████   ████████████████████" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "                                           " +
                "                                           " +
                "                                           " +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "█                                         █" +
                "████████████████████   ████████████████████";


            for (int i = 0; i < 15; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    if (i == 0 && j != 7)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                    else if (i == 14 && j != 7)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                    else if (i != 7 && j == 0)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                    else if (i != 7 && j == 14)
                    {
                        this.PositionList.Add(new Wall(i, j));
                        this.NbWall += 1;
                    }
                }
            }
            Map outMap = new Map3(this.Hero);
            this.PositionList.Add(new Exit(14, 7, outMap));
            this.NbWall += 1;
            outMap.PositionList.Add(new Exit(0, 7, this));
            outMap.NbWall += 1;
            Map outMap2 = new Map4(this.Hero);
            this.PositionList.Add(new Exit(0, 7, outMap2));
            this.NbWall += 1;
            outMap2.PositionList.Add(new Exit(14, 7, this));
            outMap2.NbWall += 1;
            Map outMap3 = new Map5(this.Hero);
            this.PositionList.Add(new Exit(7, 0, outMap3));
            this.NbWall += 1;
            outMap3.PositionList.Add(new Exit(7, 14, this));
            outMap3.NbWall += 1;
        }
    }
}
