﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Merchant : INPC
    {
        public string Name { get; set; }
        public string Initial { get; set; }
        public string Sentence { get; set; }
        public List<IEquipment> Inventory { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public Merchant(string name, int x, int y, List<IEquipment> inventory)
        {
            this.Initial = "M";
            this.Name = name;
            this.Sentence = "Hi, What can I do for you ?";
            this.X = x;
            this.Y = y;
            this.Inventory = inventory;
        }
    }
}
