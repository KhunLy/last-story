﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Lib
    {
        public static void GameInfo(string str)
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(10, 6);
            Console.WriteLine("                                                            ");
            Console.SetCursorPosition(10, 6);
            Console.WriteLine(str);
        }

        public static void Action1(string str)
        {
            int LWhiteSpace = (35 - str.Count()) / 2 - 1;
            int RWhiteSpace = str.Count() % 2 == 0 ? LWhiteSpace + 1 : LWhiteSpace;
            string lspc = "";
            for (int i = 0; i < LWhiteSpace; i++)
            {
                lspc += " ";
            }
            string rspc = "";
            for (int i = 0; i < RWhiteSpace; i++)
            {
                rspc += " ";
            }
            
            Console.SetCursorPosition(6, 46);
            Console.WriteLine("┌─────────────────────────────────┐");
            Console.SetCursorPosition(6, 47);
            Console.WriteLine("│");
            Console.SetCursorPosition(40, 47);
            Console.WriteLine("│");
            Console.SetCursorPosition(6, 48);
            Console.WriteLine($"│{lspc}{str}{rspc}│");
            Console.SetCursorPosition(6, 49);
            Console.WriteLine("│");
            Console.SetCursorPosition(40, 49);
            Console.WriteLine("│");
            Console.SetCursorPosition(6, 50);
            Console.WriteLine("└─────────────────────────────────┘");
            Console.ForegroundColor = ConsoleColor.Blue;
        }
        public static void Action2(string str)
        {
            int LWhiteSpace = (35 - str.Count()) / 2 - 1;
            int RWhiteSpace = str.Count() % 2 == 0 ? LWhiteSpace + 1 : LWhiteSpace;
            string lspc = "";
            for (int i = 0; i < LWhiteSpace; i++)
            {
                lspc += " ";
            }
            string rspc = "";
            for (int i = 0; i < RWhiteSpace; i++)
            {
                rspc += " ";
            }
            Console.SetCursorPosition(41, 46);
            Console.WriteLine("┌─────────────────────────────────┐");
            Console.SetCursorPosition(41, 47);
            Console.WriteLine("│");
            Console.SetCursorPosition(75, 47);
            Console.WriteLine("│");
            Console.SetCursorPosition(41, 48);
            Console.WriteLine($"│{lspc}{str}{rspc}│");
            Console.SetCursorPosition(41, 49);
            Console.WriteLine("│");
            Console.SetCursorPosition(75, 49);
            Console.WriteLine("│");
            Console.SetCursorPosition(41, 50);
            Console.WriteLine("└─────────────────────────────────┘");
            Console.ForegroundColor = ConsoleColor.Blue;
        }
        public static void Action3(string str)
        {
            int LWhiteSpace = (35 - str.Count()) / 2 - 1;
            int RWhiteSpace = str.Count() % 2 == 0 ? LWhiteSpace + 1 : LWhiteSpace;
            string lspc = "";
            for (int i = 0; i < LWhiteSpace; i++)
            {
                lspc += " ";
            }
            string rspc = "";
            for (int i = 0; i < RWhiteSpace; i++)
            {
                rspc += " ";
            }
            Console.SetCursorPosition(76, 46);
            Console.WriteLine("┌─────────────────────────────────┐");
            Console.SetCursorPosition(76, 47);
            Console.WriteLine("│");
            Console.SetCursorPosition(110, 47);
            Console.WriteLine("│");
            Console.SetCursorPosition(76, 48);
            Console.WriteLine($"│{lspc}{str}{rspc}│");
            Console.SetCursorPosition(76, 49);
            Console.WriteLine("│");
            Console.SetCursorPosition(110, 49);
            Console.WriteLine("│");
            Console.SetCursorPosition(76, 50);
            Console.WriteLine("└─────────────────────────────────┘");
            Console.ForegroundColor = ConsoleColor.Blue;
        }
        public static void Action4(string str)
        {
            int LWhiteSpace = (35 - str.Count()) / 2 - 1;
            int RWhiteSpace = str.Count() % 2 == 0 ? LWhiteSpace + 1 : LWhiteSpace;
            string lspc = "";
            for (int i = 0; i < LWhiteSpace; i++)
            {
                lspc += " ";
            }
            string rspc = "";
            for (int i = 0; i < RWhiteSpace; i++)
            {
                rspc += " ";
            }
            Console.SetCursorPosition(111, 46);
            Console.WriteLine("┌─────────────────────────────────┐");
            Console.SetCursorPosition(111, 47);
            Console.WriteLine("│");
            Console.SetCursorPosition(145, 47);
            Console.WriteLine("│");
            Console.SetCursorPosition(111, 48);
            Console.WriteLine($"│{lspc}{str}{rspc}│");
            Console.SetCursorPosition(111, 49);
            Console.WriteLine("│");
            Console.SetCursorPosition(145, 49);
            Console.WriteLine("│");
            Console.SetCursorPosition(111, 50);
            Console.WriteLine("└─────────────────────────────────┘");
            Console.ForegroundColor = ConsoleColor.Blue;
        }

        public static void ActionClear()
        {
            for (int i = 46; i < 51; i++)
            {
                for (int j = 6; j < 146; j++)
                {
                    Console.SetCursorPosition(j, i);
                    Console.WriteLine(" ");
                }
            }
        }
        public static void DisplayCurrentHP(Character c)
        {
            if (c is Hero)
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.SetCursorPosition(63, 16);
                Console.WriteLine("                    ");
                Console.SetCursorPosition(63, 16);
                if (c.CurrentHP <= c.HP / 5)
                {
                    if (c.CurrentHP <= 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("0");
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.Write("/" + c.HP.ToString());
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(c.CurrentHP.ToString());
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.Write("/" + c.HP.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("{0}/{1}", c.CurrentHP, c.HP);
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.SetCursorPosition(109, 16);
                Console.WriteLine("                    ");
                Console.SetCursorPosition(109, 16);
                if (c.CurrentHP < c.HP / 5)
                {
                    if (c.CurrentHP <= 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("0");
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.Write("/" + c.HP.ToString());
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(c.CurrentHP.ToString());
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.Write("/" + c.HP.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("{0}/{1}", c.CurrentHP, c.HP);
                }
            }
        }
        public static void DisplayLoot(Hero h)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.SetCursorPosition(62, 20);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(62, 20);
            Console.WriteLine(h.Wallet);
            Console.SetCursorPosition(68, 21);
            Console.WriteLine("                    ");
            Console.SetCursorPosition(68, 21);
            Console.WriteLine(h.Bag);
            Console.ForegroundColor = ConsoleColor.Blue;
        }
    }
}