﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TheLastStory.models
{
    class Fight
    {
        public Hero H { get; set; }
        public Monster M { get; set; }
        public Dice D { get; set; }
        public MediaPlayer MP { get; set; }

        public Fight(Hero h, Monster m, Dice d)
        {
            this.H = h;
            this.M = m;
            this.D = d;
            this.MP = new MediaPlayer();
        }

        public void Start()
        {
            System.Media.SoundPlayer player1 = new System.Media.SoundPlayer();
            player1.SoundLocation = "../../music/18-battle-2.wav";
            player1.Load();
            player1.Play();
            MP.Open(new Uri("../../music/SaberOn.wav", UriKind.Relative));
            MP.Play();
            Lib.GameInfo(String.Format("A {0} appears", M.Type));
            ConsoleKey k = Console.ReadKey(true).Key;
            Boolean turn = true;
            while (M.CurrentHP > 0 && H.CurrentHP > 0)
            {
                if (k == ConsoleKey.Spacebar)
                {
                    if (turn)
                    {
                        turn = false;
                        Lib.DisplayCurrentHP(M);
                        Lib.GameInfo(H.Hit(M, D.ThrowDice()).ToString() + " damage done !!");
                        Lib.DisplayCurrentHP(M);
                        MP.Open(new Uri("../../music/SlowSabr.wav", UriKind.Relative));
                        MP.Play();
                    }
                    else
                    {
                        turn = true;
                        Lib.DisplayCurrentHP(H);
                        Lib.GameInfo(M.Hit(H, D.ThrowDice()).ToString() + " damage taken !!");
                        Lib.DisplayCurrentHP(H);
                        MP.Open(new Uri("../../music/ouch.wav", UriKind.Relative));
                        MP.Play();
                    }
                }
                k = Console.ReadKey(true).Key;
            }
            if (M.CurrentHP <= 0)
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer();
                player.SoundLocation = "../../music/109-triumph.wav";
                player.Load();
                player.Play();
                M.Alive = false;
                Console.SetCursorPosition((M.X) * 3 + 8, (M.Y) * 2 + 12);
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.Write(M.Initial);
                Console.ForegroundColor = ConsoleColor.Blue;
                Lib.GameInfo("You Defeat the " + M.Type + "!!");
                System.Threading.Thread.Sleep(5000);
                Lib.GameInfo("Press any key to continue");
                Console.ReadKey(true);
                player.Stop();
            }
            else
            {
                Lib.GameInfo("GAME OVER");
                Console.ReadKey(true);
            }
        }
    }
}
