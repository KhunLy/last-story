﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.controller
{
    class UnderConstruction
    {
        public UnderConstruction(string name)
        {
            Console.Clear();
            Console.WriteLine("{0} Under Construction Back to Start", name);
            Console.ReadKey();
            Start start = new Start();
            start.Display();
        }
    }
}
