﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.controller
{
    class GameOver
    {
        public ConsoleColor ColorForeGround { get; set; }

        public GameOver()
        {
            ColorForeGround = ConsoleColor.Red;
        }

        public void Display() //10 x spc : "          " 20 x spc : "                    "
        {
            System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            player.SoundLocation = "../../music/241-game-over.wav";
            player.Load();
            player.Play();
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Clear();
            Console.WriteLine
            (
                "\n\n\n\n" +
                "                                             ┌───────────────────────────────────────────────────────────┐                                            " +
                "                                             │                                                           │                                            " +
                "                                             │                   ▄████ ▄███▄ █   █ █████                 │                                            " +
                "                                             │                   █     █   █ ██ ██ █                     │                                            " +
                "                                             │                   █     █   █ █ █ █ █                     │                                            " +
                "                                             │                   █ ███ █████ █ █ █ █████                 │                                            " +
                "                                             │                   █   █ █   █ █   █ █                     │                                            " +
                "                                             │                   █   █ █   █ █   █ █                     │                                            " +
                "                                             │                   ▀███▀ █   █ █   █ █████                 │                                            " +
                "                                             │                                                           │                                            " +
                "                                             │                                                           │                                            " +
                "                                             │                                                           │                                            " +
                "                                             │                   ▄███▄ █   █ █████ ████▄                 │                                            " +
                "                                             │                   █   █ █   █ █     █   █                 │                                            " +
                "                                             │                   █   █ █   █ █     █   █                 │                                            " +
                "                                             │                   █   █ ▀█ █▀ █████ ████                  │                                            " +
                "                                             │                   █   █  █ █  █     █ █                   │                                            " +
                "                                             │                   █   █  █ █  █     █  █                  │                                            " +
                "                                             │                   ▀███▀  ▀█▀  █████ █   █                 │                                            " +
                "                                             │                                                           │                                            " +
                "                                             └───────────────────────────────────────────────────────────┘                                            "
            );
            Console.ReadKey();
            Start start = new Start();
            start.Display();
        }
    }
}
