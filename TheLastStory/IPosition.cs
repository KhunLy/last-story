﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    interface IPosition
    {
        int X { get; set; }
        int Y { get; set; }
        string Initial { get; set; }
    }
}
