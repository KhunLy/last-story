﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLastStory.models
{
    class Dragon : Monster, IOr, ICuir
    {
        public int Cuir { get; set; }
        public int Or { get; set; }
        public Dragon(string name, int stamina, int strenght) : base(name, stamina + 1, strenght)
        {
            this.Initial = "D";
            this.Type = "Dragon";
        }
    }
}
